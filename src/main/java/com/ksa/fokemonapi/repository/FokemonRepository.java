package com.ksa.fokemonapi.repository;

import com.ksa.fokemonapi.entity.Fokemon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FokemonRepository extends JpaRepository<Fokemon, Long> {
}
