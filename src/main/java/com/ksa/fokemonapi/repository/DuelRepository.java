package com.ksa.fokemonapi.repository;

import com.ksa.fokemonapi.entity.Duel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DuelRepository extends JpaRepository<Duel, Long> {
}
