package com.ksa.fokemonapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FokeMonApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FokeMonApiApplication.class, args);
    }

}
