package com.ksa.fokemonapi.service;

import com.ksa.fokemonapi.entity.Duel;
import com.ksa.fokemonapi.entity.Fokemon;
import com.ksa.fokemonapi.model.duel.DuelInFokemonItem;
import com.ksa.fokemonapi.model.duel.DuelInFokemonResponse;
import com.ksa.fokemonapi.repository.DuelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor //Constructor --> 생성자 (종속성 자동주입)
public class DuelService {
    private final DuelRepository duelRepository;

    public void setStage(Fokemon fokemon) throws Exception {
        //몬스터 둘 이상 결투장 진입이 불가능. 현재 몬스터가 몇마리나 결투장에 있는지 알아야 함.

        List<Duel> checkList = duelRepository.findAll(); //결투장에 진입한 몬스터 리스트를 가져온다.

        if (checkList.size() >= 2) throw new Exception(); //만약 체크리스트의 갯수가 2마리 이상이면 던지기.

        Duel duel = new Duel();
        duel.setFokemon(fokemon);
        duelRepository.save(duel);
    }
    public void delDuelInFokemon(long id) {
        duelRepository.deleteById(id);
    }
    public DuelInFokemonResponse getCurrentState() {

        // 일단 결투장에 진입한 몬스터 리스트 다 가져 와. 근데 최대 2마리. 경우의 수는 0개, 1개, 2개
        List<Duel> checkList = duelRepository.findAll();

        // DuelInFokemonResponse 모양으로 무조건 줘야한다.
        DuelInFokemonResponse response = new DuelInFokemonResponse();
        // 이렇게 하면 안쪽에 칸들은 두칸 다 null
        // 두칸 다 null 이란 건 0개일 때 이미 처리하고 간다는 것

        if(checkList.size() == 2) { //체크리스트 사이즈가 2면 아래 모양으로 채워줘야 함
            response.setFokemon1(converFokemonItem(checkList.get(0))); //리스트에서 0번째 요소를 가져온다.
            response.setFokemon2(converFokemonItem(checkList.get(1)));

        } else if (checkList.size() == 1) { //체크리스트 사이즈가 1이면
            response.setFokemon1(converFokemonItem(checkList.get(0))); // 어차피 두번째 자리는 null이 될거니까

        }
        return response;

    }
    private DuelInFokemonItem converFokemonItem(Duel duel) {
        DuelInFokemonItem fokemonItem = new DuelInFokemonItem();
        fokemonItem.setDuelFokemonId(duel.getId());
        fokemonItem.setFokemonId(duel.getFokemon().getId());
        fokemonItem.setFokemonName(duel.getFokemon().getName());
        fokemonItem.setImgUrl(duel.getFokemon().getImgUrl());
        fokemonItem.setPower(duel.getFokemon().getPower());
        fokemonItem.setPhysical(duel.getFokemon().getPhysical());
        fokemonItem.setDefensive(duel.getFokemon().getDefensive());
        fokemonItem.setSpeed(duel.getFokemon().getSpeed());

        return fokemonItem;
    }
}
