package com.ksa.fokemonapi.service;

import com.ksa.fokemonapi.entity.Fokemon;
import com.ksa.fokemonapi.model.fokemon.FokemonItem;
import com.ksa.fokemonapi.model.fokemon.FokemonRequest;
import com.ksa.fokemonapi.model.fokemon.FokemonResponse;
import com.ksa.fokemonapi.repository.FokemonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FokemonService {
    private final FokemonRepository fokemonRepository;

    public Fokemon getData(long id) {
        return fokemonRepository.findById(id).orElseThrow();
    }


    public void setFokemon(FokemonRequest request) {
        Fokemon addData = new Fokemon();

        addData.setName(request.getName());
        addData.setFokemonType(request.getFokemonType());
        addData.setPower(request.getPower());
        addData.setPhysical(request.getPhysical());
        addData.setDefensive(request.getDefensive());
        addData.setSpeed(request.getSpeed());
        addData.setIsGender(request.getIsGender());
        addData.setClassify(request.getClassify());
        addData.setStature(request.getStature());
        addData.setWeight(request.getWeight());
        addData.setProperty(request.getProperty());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setImgUrl(request.getImgUrl());

        fokemonRepository.save(addData);
    }

    public List<FokemonItem> getFokemons() {
        List<Fokemon> originList = fokemonRepository.findAll();
        List<FokemonItem> result = new LinkedList<>();

        for (Fokemon fokemon : originList) {
            FokemonItem addItem = new FokemonItem();
            addItem.setId(fokemon.getId());
            addItem.setName(fokemon.getName());
            addItem.setPower(fokemon.getPower());
            addItem.setPhysical(fokemon.getPhysical());
            addItem.setDefensive(fokemon.getDefensive());
            addItem.setSpeed(fokemon.getSpeed());
            addItem.setImgUrl(fokemon.getImgUrl());

            result.add(addItem);
        }

        return result;
    }

    public FokemonResponse getFokemon(long id) {
        Fokemon originData = fokemonRepository.findById(id).orElseThrow();

        FokemonResponse response = new FokemonResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setFokemonType(originData.getFokemonType().getFokemonType());
        response.setPower(originData.getPower());
        response.setPhysical(originData.getPhysical());
        response.setDefensive(originData.getDefensive());
        response.setSpeed(originData.getSpeed());
        response.setIsGender(originData.getIsGender() ? "남녀" : "불명");
        response.setClassify(originData.getClassify());
        response.setStature(originData.getStature());
        response.setStature(originData.getStature());
        response.setWeight(originData.getWeight());
        response.setProperty(originData.getProperty());
        response.setEtcMemo(originData.getEtcMemo());
        response.setImgUrl(originData.getImgUrl());

        return response;
    }
}
