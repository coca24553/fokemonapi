package com.ksa.fokemonapi.model.fokemon;

import com.ksa.fokemonapi.enums.FokemonType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FokemonRequest {
    private String name;
    @Enumerated(value = EnumType.STRING)
    private FokemonType fokemonType;
    private Double power; //공격력
    private Double physical; //체력
    private Double defensive; //방어력
    private Double speed; //스피드
    private Boolean isGender;
    private String classify;
    private Double stature;
    private Double weight;
    private String property;
    private String etcMemo;
    private String imgUrl;
}
