package com.ksa.fokemonapi.model.fokemon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FokemonResponse {
    private Long id;
    private String name;
    private String  fokemonType;
    private Double power; //공격력
    private Double physical; //체력
    private Double defensive; //방어력
    private Double speed; //스피드
    private String isGender;
    private String classify; //분류
    private Double stature; //키
    private Double weight; //몸무게
    private String property; //특성
    private String etcMemo;
    private String imgUrl;
}
