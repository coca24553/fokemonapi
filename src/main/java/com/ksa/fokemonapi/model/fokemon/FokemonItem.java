package com.ksa.fokemonapi.model.fokemon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FokemonItem {
    private Long id;
    private String name;
    private Double power; //공격력
    private Double physical; //체력
    private Double defensive; //방어력
    private Double speed; //스피드
    private String imgUrl;
}
