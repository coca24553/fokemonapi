package com.ksa.fokemonapi.model.duel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DuelRequest {
    private String duelName;
}
