package com.ksa.fokemonapi.model.duel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DuelInFokemonResponse {
    private DuelInFokemonItem fokemon1;
    private DuelInFokemonItem fokemon2;
}
