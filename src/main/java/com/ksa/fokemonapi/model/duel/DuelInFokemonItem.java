package com.ksa.fokemonapi.model.duel;

import lombok.Getter;
import lombok.Setter;

import javax.annotation.processing.Generated;

@Getter
@Setter
public class DuelInFokemonItem {
    private Long duelFokemonId;
    private Long fokemonId;
    private String fokemonName;
    private String imgUrl;
    private Double power; //공격력
    private Double physical; //체력
    private Double defensive; //방어력
    private Double speed; //스피드
}
