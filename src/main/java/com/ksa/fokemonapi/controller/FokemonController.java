package com.ksa.fokemonapi.controller;

import com.ksa.fokemonapi.model.*;
import com.ksa.fokemonapi.model.fokemon.FokemonItem;
import com.ksa.fokemonapi.model.fokemon.FokemonRequest;
import com.ksa.fokemonapi.model.fokemon.FokemonResponse;
import com.ksa.fokemonapi.service.FokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/fokemon")
public class FokemonController {
    private final FokemonService fokemonService;

    @PostMapping("/join")
    public String setFokemon(@RequestBody FokemonRequest request) {
        fokemonService.setFokemon(request);

        return "ok";
    }
    @GetMapping("/all")
    public ListResult<FokemonItem> getFokemons(){
        List<FokemonItem> list = fokemonService.getFokemons();

        ListResult<FokemonItem> response = new ListResult<>();
        response.setList(list);
        response.setMsg("성공하였습니다.");
        response.setCode(0);
        response.setTotalCount(list.size());

        return response;
    }


    @GetMapping("/detail/{id}")
    public SingleResult<FokemonResponse> getDetail(@PathVariable long id){
        FokemonResponse result = fokemonService.getFokemon(id);

        SingleResult<FokemonResponse> response = new SingleResult<>();
        response.setMsg("성공하였습니다.");
        response.setCode(0);
        response.setData(result);

        return response;
    }

}
