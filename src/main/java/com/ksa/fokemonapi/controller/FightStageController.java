package com.ksa.fokemonapi.controller;

import com.ksa.fokemonapi.entity.Fokemon;
import com.ksa.fokemonapi.model.CommonResult;
import com.ksa.fokemonapi.model.SingleResult;
import com.ksa.fokemonapi.model.duel.DuelInFokemonResponse;
import com.ksa.fokemonapi.service.DuelService;
import com.ksa.fokemonapi.service.FokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/fight-stage")
public class FightStageController {
    private final DuelService duelService;
    private final FokemonService fokemonService;

    @PostMapping("/stage/in/monster-id/{monsterId}")
    public CommonResult setMonsterStageIn(@PathVariable long monsterId) throws Exception {
        Fokemon fokemon = fokemonService.getData(monsterId);
        duelService.setStage(fokemon);

        CommonResult result = new CommonResult();
        result.setMsg("성공");
        result.setCode(0);

        return result;
    }

    @GetMapping("/current/state")
    public SingleResult<DuelInFokemonResponse> getCurrentState() {
        SingleResult<DuelInFokemonResponse> result = new SingleResult<>();
        result.setMsg("성공");
        result.setCode(0);
        result.setData(duelService.getCurrentState());

        return result;
    }

    @DeleteMapping("/stage/out/stage-id/{stageId}")
    public CommonResult delStageOutMonster(@PathVariable long stageId) {
        duelService.delDuelInFokemon(stageId);

        CommonResult result = new CommonResult();
        result.setMsg("성공");
        result.setCode(0);

        return result;
    }
}
