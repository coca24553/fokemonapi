package com.ksa.fokemonapi.entity;

import com.ksa.fokemonapi.enums.FokemonType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Fokemon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private FokemonType fokemonType;

    @Column(nullable = false)
    private Double power; //공격력

    @Column(nullable = false)
    private Double physical; //체력

    @Column(nullable = false)
    private Double defensive; //방어력

    @Column(nullable = false)
    private Double speed; //스피드

    @Column(nullable = false)
    private Boolean isGender;

    @Column(nullable = false, length = 15)
    private String classify; //분류

    @Column(nullable = false)
    private Double stature; //키

    @Column(nullable = false)
    private Double weight; //몸무게

    @Column(nullable = false, length = 20)
    private String property; //특성

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(nullable = false)
    private String imgUrl;
}
