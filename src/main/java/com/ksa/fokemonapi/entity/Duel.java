package com.ksa.fokemonapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Duel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "fokemonId", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Fokemon fokemon;
}
